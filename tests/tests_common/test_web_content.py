import unittest

import bs4

import virden_jobs.common.web_content


class WebContentTestCase(unittest.TestCase):

    def setUp(self):
        self.html = '''
<html><body>
<div class="test1"><a href="https://job">job</a></div>
<div class="test2"><a href="https://job2">job2</a></div>
<div class="test1"><a href="https://job3">job3</a></div>
</body></html>
'''
        self.web_content = virden_jobs.common.web_content._WebContent(
            bs4.BeautifulSoup(self.html, 'html.parser'))

    def test_find_all_should_return_list(self):
        found = self.web_content.find_all('div', class_='test1')

        [self.assertIsInstance(
            x, virden_jobs.common.web_content._WebContent) for x in found]

    def test_get_should_return_value(self):
        found = self.web_content.find_all('a')
        urls = [url.get('href') for url in found]

        lst = ['https://job', 'https://job2', 'https://job3']
        self.assertListEqual(lst, urls)


if __name__ == '__main__':
    unittest.main()
