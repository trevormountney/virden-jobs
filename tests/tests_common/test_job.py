import unittest

import virden_jobs.common.job


class JobTestCase(unittest.TestCase):

    def setUp(self):
        self.job = virden_jobs.common.job.Job('job', 'https://job')

    def test_init_should_create_object(self):
        self.assertIsInstance(self.job, virden_jobs.common.job.Job)

    def test_job_should_have_attributes(self):
        self.assertEqual(self.job.description, 'job')
        self.assertEqual(self.job.url, 'https://job')
        self.assertEqual(self.job.company, '')
        self.job.company = 'company'
        self.assertEqual(self.job.company, 'company')

    def test_job_should_equal(self):
        equivalent_job = virden_jobs.common.job.Job('job', 'https://job')
        self.assertEqual(self.job, equivalent_job)

    def test_job_should_not_equal(self):
        inequivalent_job = virden_jobs.common.job.Job('job', 'https://site')
        self.assertNotEqual(self.job, inequivalent_job)

    def test_job_should_return_string(self):
        self.assertEqual(str(self.job), 'job https://job ')
        self.job.company = 'company'
        self.assertEqual(str(self.job), 'job https://job company')


if __name__ == '__main__':
    unittest.main()
