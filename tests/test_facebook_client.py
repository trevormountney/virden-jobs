import unittest
from unittest.mock import patch

import virden_jobs.facebook_client


class FacebookTestCase(unittest.TestCase):

    @patch('facebook.GraphAPI')
    def setUp(self, mock):
        self.facebook = virden_jobs.facebook_client.Facebook()
        self.facebook.connect('token')

    def test_get_page_posts_should_return_posts(self):
        self.facebook._facebook.get_object.return_value = {
            'data': [{'message': 'post1'}, {'message': 'post2'}]}
        posts = self.facebook.get_page_posts('page_id')

        self.assertEqual(posts[0]['message'], 'post1')

    def test_post_should_return_true(self):
        self.assertTrue(self.facebook.try_post('page_id', 'message')[0])


if __name__ == '__main__':
    unittest.main()
