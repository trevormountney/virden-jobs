import unittest
from unittest.mock import patch, MagicMock
from testfixtures import LogCapture

import virden_jobs.common.job
import virden_jobs.facebook_client
import virden_jobs.main


def iter_modules():
    yield ('module_loader', 'vjobs_test', 'ispkg')


def mock_plugin_run():
    job = virden_jobs.common.job.Job(description='job', url='https://job')
    job2 = virden_jobs.common.job.Job(description='job2', url='https://job2')
    return [job, job2]


def get_job_mock(description='job', url='https://job', company=''):
    mock = MagicMock(spec=virden_jobs.common.job.Job)
    mock.description = description
    mock.url = url
    mock.company = company
    return mock


class MainTestCase(unittest.TestCase):

    @patch('pkgutil.iter_modules', side_effect=iter_modules)
    def test_get_plugins_should_return_dict(self, mock):
        plugins = virden_jobs.main.get_plugins()
        self.assertTrue('vjobs_test' in plugins)

    def test_get_formatted_message_should_return_string(self):
        value = 'job  https://job'
        mock = get_job_mock()
        message = virden_jobs.main._get_formatted_message(mock)
        self.assertEqual(message, value)

    def test_is_job_should_return_true(self):
        mock = get_job_mock()
        self.assertTrue(virden_jobs.main._is_job(mock))

    def test_is_job_should_return_false(self):
        mock = get_job_mock()
        del mock.url
        self.assertFalse(virden_jobs.main._is_job(mock))

    def test_get_jobs_should_return_list(self):
        mock = MagicMock()
        mock.run = mock_plugin_run
        mock_plugins = {'vjobs_test': mock}
        jobs = virden_jobs.main.get_jobs(mock_plugins)

        self.assertListEqual(jobs, mock_plugin_run())

    def test_is_existing_job_should_return_true(self):
        job = get_job_mock(description='job', url='https://job')
        posts = [{'message': 'job  https://url'},
                 {'message': 'job  https://job'}]
        self.assertTrue(virden_jobs.main._is_existing_job(posts, job))

    def test_is_existing_job_should_return_false(self):
        job = get_job_mock(description='job', url='https://job')
        posts = [{'message': 'job  https://url'},
                 {'message': 'job  https://site'}]
        self.assertFalse(virden_jobs.main._is_existing_job(posts, job))

    def test_post_jobs_to_facebook_should_write_to_log(self):
        mock = MagicMock(spec=virden_jobs.facebook_client.Facebook)
        posts = [{'message': 'job  https://url'},
                 {'message': 'job  https://job'}]
        jobs = mock_plugin_run()

        with LogCapture() as capture:
            virden_jobs.main.post_jobs_to_facebook(posts, jobs, mock, 'pageid')
            log = (('root', 'INFO', 'Entry already present: job https://job '),
                   ('root', 'INFO', 'Posted job: job2 https://job2 '))

            capture.check(*log)


if __name__ == '__main__':
    unittest.main()
